﻿# ODS QoS Tool Release Notes

### V1.002a - 240110
  1. Firmware Reference  v2.5.0.0a
  2. Platform Reference v1.4.5.0
   
### V1.001a - 240110
  1. Firmware Reference v2.4.4.2a
  2. Platform Reference v1.4.5.0
  3. Bugfix - Triggering locates on motion instead of sampling interval.

### V1.000a - 231209
  1. Firmware Reference  v2.4.4.2a
  2. Initial Implementation
