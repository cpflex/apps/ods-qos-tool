"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Run = void 0;
async function Run(o, _device) {
  // The raw data received from the device. Should not be edited.
  const orgData = o.data.original.data;

  let nid = "";
  if (typeof orgData.nid !== "undefined") {
    nid = orgData.nid;
  }

  switch (nid) {
    //-------------------------------------
    // Duress Notification (1 click)
    //-------------------------------------
    case "ODS.Event.Duress": {
      if (orgData.svals[0] === "ODS.Event.Duress.Activated") {
        const notificationSetting = {
          body:
            _device.name +
            " device duress button has been activated. Response is required.",
          // eslint-disable-next-line max-len
          icon: "https://s3.eu-west-1.amazonaws.com/v3.thinxmate.com/pictures/5968e243-1538-49d4-976b-dac3cd8e6687-alert-512.png",
          title: "Duress Alarm",
        };
        await sharing.alarm.insert(
          {
            ...o,
            deviceId: _device.id,
            device: _device,
            ruleId: _rule.id,
            userId: _device.userId || 0,
          },
          orgData?.rule?.json.message || "Duress Alarm",
          notificationSetting
        );
        _device.signals.set("Mode", "Duress Alarm");
        _device.signals.set(
          "Status",
          "Device duress button pressed.  Response is required."
        );
      } else if (orgData.svals[0] === "ODS.Event.Duress.Deactivated") {
        await sharing.alarm.dissolve(_device.id, _rule.id, o);
        _device.signals.set("Mode", "Duress Alarm Cancelled");
        _device.signals.set(
          "Status",
          "Duress alarm cancelled by device user.  No further response required."
        );
      }
      break;
    }

    //-------------------------------------
    // Notification #1 signal (3 second press)
    //-------------------------------------
    case "ODS.Event.Notify1": {
      const notificationSetting = {
        body: _device.name + " device notify1 signal has been activated.",
        // eslint-disable-next-line max-len
        icon: "https://s3.eu-west-1.amazonaws.com/v3.thinxmate.com/pictures/5968e243-1538-49d4-976b-dac3cd8e6687-alert-512.png",
        title: "Device Notification #1",
      };
      await sharing.alarm.insert(
        {
          ...o,
          deviceId: _device.id,
          device: _device,
          ruleId: _rule.id,
          userId: _device.userId || 0,
        },
        orgData?.rule?.json.message || "Device Notification #1",
        notificationSetting
      );
      _device.signals.set("Mode", "Device Notification #1");
      _device.signals.set(
        "Status",
        "Notification #1 button sequence entered by device user."
      );
      break;
    }
  }
}
exports.Run = Run;
