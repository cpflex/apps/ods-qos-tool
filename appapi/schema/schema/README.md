# Additional Schemas
The following schemas are included from the V1 SDK.


* system.ocm.json
* power.ocm.json
* location.ocm.json
* tracking.ocm.json

Please see the SDK for definitions.