/**
 *  Name:  app.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2019-2023 Codepoint Technologies
 *  All Rights Reserved
 */

#include "ui.i"
#include "timer.i"
#include "log.i"
#include "telemetry.i"
#include "NvmRecTools.i"
#include "system.i"
#include "power.i"
#include "OcmNidDefinitions.i"

const {
	EVENT_LATENCYCHECK=0
}


/**
* @brief  Application Defined callback handler that indicates entering power savings.
* @remarks Required implementation to catch enter power saving events.
*/
forward app_EnterPowerSaving(pctCharged);

/**
* @brief  Application Defined callback handler that indicates entering power savings.
* @remarks Required implementation to catch exit power saving events.
*/
forward app_ExitPowerSaving(pctCharged);