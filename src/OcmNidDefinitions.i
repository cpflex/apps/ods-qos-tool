/*******************************************************************************
*                !!! OCM CODE GENERATED FILE -- DO NOT EDIT !!!
*
* ODS QoS Sampling Tool Protocol Schema
*  nid:        OdsQosTool
*  uuid:       a41b1854-33fb-4679-b0a5-5eacc10f530a
*  ver:        1.0.0.0
*  date:       2023-12-07T22:56:14.487Z
*  product: Cora Tracking CT100X
* 
*  Copyright 2023 - Codepoint Technologies, Inc. All Rights Reserved.
* 
*******************************************************************************/
const OcmNidDefinitions: {
	NID_LocConfigPmode = 1,
	NID_SystemLogAll = 2,
	NID_LocConfigPmodeDefault = 3,
	NID_TrackConfig = 4,
	NID_SystemLogDisable = 5,
	NID_PowerBattery = 6,
	NID_QosEvent = 7,
	NID_LocConfigPtech = 8,
	NID_SystemReset = 9,
	NID_PowerChargerCritical = 10,
	NID_TrackConfigNomintvl = 11,
	NID_SystemConfigPollintvl = 12,
	NID_SystemLog = 13,
	NID_TrackMode = 14,
	NID_SystemLogAlert = 15,
	NID_LocConfigPmodePrecise = 16,
	NID_LocConfigPtechBle = 17,
	NID_LocConfigPtechWifi = 18,
	NID_TrackModeActive = 19,
	NID_LocConfig = 20,
	NID_LocConfigPmodeMedium = 21,
	NID_PowerCharger = 22,
	NID_LocConfigPmodeCoarse = 23,
	NID_TrackModeDisabled = 24,
	NID_TrackConfigEmrintvl = 25,
	NID_SystemErase = 26,
	NID_LocConfigPtechWifible = 27,
	NID_LocConfigPtechAutomatic = 28,
	NID_PowerChargerCharging = 29,
	NID_SystemLogInfo = 30,
	NID_QosEventLatencycheck = 31,
	NID_TrackConfigAcquire = 32,
	NID_TrackConfigInactivity = 33,
	NID_SystemLogDetail = 34,
	NID_PowerChargerCharged = 35,
	NID_SystemConfig = 36,
	NID_PowerChargerDischarging = 37,
	NID_TrackModeEnabled = 38
};
