/**
*  Name:  ConfigTrackingEngine.i
*
*  This module is the proprietary property of Codepoint Technologies, Inc.
*  Copyright (C) 2023 Codepoint Technologies, Inc.
*  All Rights Reserved
*
* Description:
*  Configuration constants for network link Checks.
**/

#include "ui.i"
#include "telemetry.i"
#include <OcmNidDefinitions.i>
#include "TrackingEngineDefs.i"


/*******************************************************************************
* Tracking Engine General Configuration
*******************************************************************************/
//NID Command Map, by default these map to ped_tracker protocol spec.
//If your protocol is different, update the NID mappings.

const _: {
	NM_CMD_CONFIG	= _: NID_TrackConfig,
	NM_INACTIVITY	= _: NID_TrackConfigInactivity,
	NM_NOMINTVL 	= _: NID_TrackConfigNomintvl,
	NM_EMRINTVL		= _: NID_TrackConfigEmrintvl,
	
	NM_CMD_TRACKING	= _: NID_TrackMode,
	NM_ACTIVE		= _: NID_TrackModeActive,
	NM_ACQUIRE		= _: NID_TrackConfigAcquire,
	NM_ENABLE		= _: NID_TrackModeEnabled,
	NM_DISABLE		= _: NID_TrackModeDisabled	
};

const bool:     TRACKING_INIT_ACTIVATE = true;  // If true, tracking is initially active.

// Acquire positioning data indicator
const 			TRACKING_IND_LED_ACTIVATE    = LED1;
const 			TRACKING_IND_LED_DEACTIVATE  = LED2;
const 			TRACKING_IND_COUNT = 4;
const  bool:    TRACKING_IND_ENABLE = false;	//Tracking activation / deactivation 
                                                //indicator is disabled, since always active.
const  bool:	ACQUIRE_IND_ENABLE	= true;		//Acquire indicator Enabled
const			ACQUIRE_IND_LED		= LED1;		//LED to use for indication.
const			ACQUIRE_IND_COUNT	= 2;		//Number of cycles to blink.
const			ACQUIRE_IND_ON		= 466;		//Blink on-time in milliseconds.
const			ACQUIRE_IND_OFF		= 133;		//Blink off-time in milliseconds.

// Persisted variable default values.
//These values define the initial value for persisted configurations.
//These are set when the application is first run after installation.
const DEFAULT_ACQUIRE = 50;		//Defines the movement threshold (mg) to begin acquisition.
const DEFAULT_INACTIVITY = 100;     //Defines movement threshold (mg) to test for innactivity
const DEFAULT_INTVL_NORMAL = 1;     //Defines the reporting interval in minutes
const DEFAULT_INTVL_EMERGENCY = 5;   //Defines the reporting interval in minutes

const  TelemPostFlags: DEFAULT_FLAGS_POSTACQUIRE = TPF_ARCHIVE;

/*******************************************************************************
* Tracking Engine State and Properties
*******************************************************************************/

/*******************************
* @brief Enum defines the motion tracking states
********************************/
const TE_STATEDEFS_COUNT = 4;
const  {
	TE_Idle = 0,             //Tracking is active waiting for motion
    TE_ActiveLocate = 1,   // Active tracking, Locate on enter
    TE_ActiveWaiting = 2,   // Active tracking waiting 30 seconds 
    TE_InnactiveWaiting = 3      //Performs locate on enter, if no more motion, goes to exit tracking, otherwise return to Tracking Locate.
};

/*******************************
* @brief Enum User friendly names of the state.
********************************/
stock const  TE_StateNames[TE_STATEDEFS_COUNT]{5} = [ 
	"IDL", "LOC", 
	"LWT", "IWT"
];

/*******************************
* @brief Enum defines Properties define in the Tracking engine.
********************************/
const TE_PROPERTIES_COUNT=2; 
const {
    TEPROP_ACTIVE_WAIT = 0,     //Activate wait period in seconds.
    TEPROP_INNACTIVE_WAIT = 1   //Innactive wait period in seconds.
};

//*******************************
// @brief 1-D array defines properties array (numerically indexed starting at 0).
// @remarks A maximum of 15 properties may be defined.
//*******************************
stock const TE_PROPERTIES_DEFAULT [TE_PROPERTIES_COUNT] = [
    20,   //Default is 20 seconds
    600   //Default is 600 seconds (10 minutes.)
];

/*******************************************************************************
* Tracking Engine State Definitions
*******************************************************************************/

/*******************************
* @brief 2-D array defines the tracking state machine.
********************************/
stock const TE_STATEDEFS [TE_STATEDEFS_COUNT] [
    .flags,  //General State configuration flags.
    .track,  //Tracking configuration flags.
    .motion,  //Motion detection configuration flags.
    .report  //Reporting configuration flags.
] = [

    //STATE TE_Idle - Wait for Motion (1 sample, Motion Enabled)                           
    [  
        TESF_CallbackOnEnter | TESF_CallbackOnExit,  // Signals Entering and exiting idle condition.
        0,  //Tracking is disabled
        0,  //Motion disable.
        0,  //Reporting disabled.
    ],

    //STATE TE_ActiveLocate - //Execute a locate on Enter and Sampling every 60 seconds.  
    [
        0,  
        TETF_LocateOnEnter, 
        0, // Motion sensing disabled during this state
        TERPT_Enabled | TERPT_RptNoMeasurements | TERPT_POST_SET( TPF_NONE) | TERPT_PRIORITY_SET(MP_med) 
    ], 

    //STATE TE_ActiveWaiting - //Wait for the specified interval and check if there is motion.
    //Leave reporting enabled so we get updates.
    [
        TESF_SAMPLEINTERVAL_SET(0xFFF0),  //Map interval to property 0.
        TETF_Enabled,
        TEMF_Enabled | TEMF_ProfileWakeup | TEMF_ResNormal | TEMF_Sampling1Hz | TEMF_DURATION_SET(1) | TEMF_THRESHOLD_SET(100),
        TERPT_Enabled | TERPT_RptNoMeasurements | TERPT_POST_SET( TPF_NONE) | TERPT_PRIORITY_SET(MP_med) 
    ],     

    //STATE TE_InnactiveWaiting  No motion detected, report location and wait one more time.
    [
        TESF_SAMPLEINTERVAL_SET(0xFFF1),  //Map interval to property 1.
        TETF_LocateOnEnter,
        TEMF_Enabled | TEMF_ProfileWakeup | TEMF_ResNormal | TEMF_Sampling1Hz | TEMF_DURATION_SET(1) | TEMF_THRESHOLD_SET(100),
        TERPT_Enabled | TERPT_RptNoMeasurements | TERPT_POST_SET( TPF_NONE) | TERPT_PRIORITY_SET(MP_med) 
    ]
];


/*******************************************************************************
* Tracking Engine Trigger Definitions
*******************************************************************************/

const TE_TRIGGERDEFS_COUNT = 6;
stock const TE_TRIGGERDEFS [TE_TRIGGERDEFS_COUNT][
    .flags,
    .motion, 
    .duration, 
] = [

    //---------------------------------------------------
    // TE_Idle Triggers
    //---------------------------------------------------

    // TRIGGER On User1 signal evaluation. This "Activates" tracking.
    [
        TETRIG_IDXSTATE_SET(  TE_Idle ) |  TETRIG_IDXNEXT_SET( TE_ActiveLocate) |  TETRIG_SIGNALS_SET( TESIG_USER1),
        0,
        0
    ],
 
    //---------------------------------------------------
    // TE_ActiveLocate Triggers
    //---------------------------------------------------

    // TRIGGER ON Enter State, Transition to next State
    [
        TETRIG_IDXSTATE_SET( TE_ActiveLocate ) |  TETRIG_IDXNEXT_SET( TE_ActiveWaiting) |  TETRIG_SIGNALS_SET( TESIG_ENTER_STATE),
        0,
        TEME_Enabled | TEME_MINTICKS_SET(0) | TEME_MAXTICKS_SET( IGNORE_VALUE)
    ],
    
    //---------------------------------------------------
    // TE_ActiveWaiting Triggers
    //---------------------------------------------------

    // TRIGGER On Sample Interval
    //   if any motion detected within the specified sample interval, 
    //   transition to tracking locate, 
    [
        TETRIG_IDXSTATE_SET( TE_ActiveWaiting ) |  TETRIG_IDXNEXT_SET( TE_ActiveLocate) |  TETRIG_SIGNALS_SET( TESIG_SAMPLE),
        TEME_Enabled | TEME_MINTICKS_SET(1) | TEME_MAXTICKS_SET( IGNORE_VALUE),
        0
    ],
    
    // TRIGGER On Sample Interval 
    //   transition to Innactive -- only executes if prior trigger does not.
    [
        TETRIG_IDXSTATE_SET( TE_ActiveWaiting ) |  TETRIG_IDXNEXT_SET( TE_InnactiveWaiting) |  TETRIG_SIGNALS_SET( TESIG_SAMPLE),
        0,
        0
    ],  

    //---------------------------------------------------
    // TE_InnactiveTracking Triggers
    //---------------------------------------------------

    // TRIGGER On motion detected 
    // Any motion detected, transition to tracking locate, 
    [
        TETRIG_IDXSTATE_SET( TE_InnactiveWaiting ) |  TETRIG_IDXNEXT_SET( TE_ActiveLocate) |  TETRIG_SIGNALS_SET( TESIG_MOTION),
        TEME_Enabled | TEME_MINTICKS_SET(1) | TEME_MAXTICKS_SET( IGNORE_VALUE),
        0
    ],
    
    // TRIGGER On Sample no motion detected - No motion detected within the specified sample interval, transition to idle, 
    [
        TETRIG_IDXSTATE_SET( TE_InnactiveWaiting ) |  TETRIG_IDXNEXT_SET( TE_Idle) |  TETRIG_SIGNALS_SET( TESIG_SAMPLE),
        0,
        0
    ]
]; 

