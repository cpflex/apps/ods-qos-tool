/**
*  Name:  ConfigSystem.p
*
*  This module is the proprietary property of Codepoint Technologies, Inc.
*  Copyright (C) 2023 Codepoint Technologies, Inc.
*  All Rights Reserved
*
* Description:
*  Configuration constants for the battery module.
**/

/*************************
* Battery Configuration 
**************************/
#include <StatefulEventDefs.i>
#include <OcmNidDefinitions.i>

const SE_DEF_STATECOUNT = 1;

//FIELDS ARE nidMsg, flags, nidSet, nidClear, priority, category, initialState
stock SE_EVENTDEFS[SE_DEF_STATECOUNT][.nidMsg, .flags, .nidSet, .nidClear, .priority, .category, .initialState] = [
	//Emergency Mode.
	[ 
		_: NID_QosEventLatencycheck, 
		_: (SE_CONFIG_TRANSIENT), //Does not send state information.
		_: 0, 
		_: 0, 
		_: MP_med, 
		_: MC_info,
		_: SE_UNKNOWN		
	] 
];


