# ODS QoS Tool
**Version: 1.002a**<br/>
**Release Date: 2024/1/11**

This application provides QoS Testing tools to facilitate rapid survey of facilities after deployments or update to the infrastructure.

Click here for [Release Notes](./ReleaseNotes.md).

## Key Features  
The tracker application is designed to rapid location sampling and user triggered uplinks when activated and in motion.

Key features include:

1. Activate (2 click) / Deactivate (3 click) Rapid Sampling (20 second locates) while moving.
2. Automatically disables rapid sampling after 10 minutes of no-motion.
4. Motion activated location data collection 
5. Cloud configurable location and tracking settings / report
6. User/Cloud controlled tracking activation / deactivation
7. Message Caching is disabled.
8. Battery status events and report
9. Location measurement configuration control for admin

# User Interface 
This section describes the essential functions of the CT1000 user interface as defined
by this application.   

## Button Commands  

The following tables summarize the button activated commands organized into primary operational and additional commands.  
### Operational Commands 
These commands are intended for operational use, providing a very simple user interface.
| **Command**                         |     **Action**       |     **Button**     |  **Description**          |
|-------------------------------------|:--------------------:|:------------------:|---------------------------|
| **Network Latency Check**           | one (1)<br>quick press     | any button or both   | Sends unconfirmed uplink (no location data) to test current network performance. | 
| **Sampling  Activation**            | two (2)<br>quick presses   | any button or both   | Activate rapid sampling. |  
| **Sampling Deactivation**           | three (3)<br>quick presses | any button or both   | Deactivate rapid sampling.  |  

### Additional Commands
For testing and device management, the following commands are available but not required
for ordinary operational use.
| **Command**                         |     **Action**       |     **Button**     |  **Description**           |
|-------------------------------------|:--------------------:|:------------------:|---------------------------|
| **Battery Status**                  | five (5) <br>quick presses        | Button #2       | Indicates battery level <br> [*See Battery Indicator*](#battery-indicator) 
| **Network Reset**                   | hold > 15<br>seconds              | any button or both | Network reset device causes rejoin.     |
| **Factory Reset**                   | hold > 25<br>seconds              | any button or both | Resets the network and configuration to factory defaults for the application.     |
 
## LED Descriptions

The following table indicates the meaning of the two bi-color LEDs on the device.  The LED's are
positioned:

* **LED #1** - Upper LED, furthest away from charger pads
* **LED #2** - Lower LED, closest to charger pads 

| **Description**                |                **Indication**               |   **LEDS**  |
|--------------------------------|:-------------------------------------------:|:-----------:|
| **Sampling Activating**        |  Green blink <br>ten (5) times quickly      |    LED #2   |
| **Sampling Active**            |  Two (2) green blink once every 5 seconds   |    LED #2   |
| **Sampling Deactivating**      |  Orange blink <br>ten (5) times quickly     |    LED #2   |
| **Device Reset**               |       Green blink <br>three (3) times       |     both    |
| **Battery Charging**           | Orange slow blink every<br>ten (10) seconds |    LED #1   |
| **Battery Fully Charged**      |  Green slow blink every<br>ten (10) seconds |    LED #1   |


## Battery Indicator 

Activating the battery status commend will indicate the percent charge of the battery in 20% increments.

LED #2 will blink as follows to indicate percent charge.

| LED #2   | % Charge    | Description|
|----------|-------------|------------|
|  1 red   |   < 5%      | Battery level is critical,  charge immediately. |
|  1 green |  5%  - 30%  | Battery low  less than 30% remaining. |
|  2 green |  30% - 50%  | Battery is about 40% charged. |
|  3 green |  50% - 70%  | Battery is about 60% charged. |
|  4 green |  70% - 90%  | Battery  is about 80% charged. |
|  5 green |  > 90%      | Battery is fully charged. |

The tag will report battery status when connect or disconnect from charging. The battery level and temperature are monitored every 10 minutes. The tag will also report battery status when its level change more than 5%. The tag will go to low power mode and stop automatic location reporting when battery level is below 20%. The tag will resume automatic location reporting after battery 
level return to above 25%. 

## Settings ##

App supports non-volatile downlink settings for standard/emergency reporting intervals and accelerometer parameters.

  These values survive reboot or dead battery conditions.


---
*Copyright 2023-2024, Codepoint Technologies, Inc.* <br>
*All Rights Reserved*
